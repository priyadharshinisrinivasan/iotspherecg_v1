package com.ey.iot.iotsphereeg.beans;

import java.util.Date;

public class Device implements Comparable<Device> {
	private String deviceId;
	private String deviceStatus;
	private String gatewayId;
	private String connectionStateUpdatedTime;
	private Date lastActivityTime;
	private String statusUpdatedTime;
	//private Date lastActivityTime;
	
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceStatus() {
		return deviceStatus;
	}
	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}
	public String getGatewayId() {
		return gatewayId;
	}
	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}
	public String getConnectionStateUpdatedTime() {
		return connectionStateUpdatedTime;
	}
	public void setConnectionStateUpdatedTime(String connectionStateUpdatedTime) {
		this.connectionStateUpdatedTime = connectionStateUpdatedTime;
	}
	public Date getLastActivityTime() {
		return lastActivityTime;
	}
	public void setLastActivityTime(Date lastActivityTime) {
		this.lastActivityTime = lastActivityTime;
	}
	public String getStatusUpdatedTime() {
		return statusUpdatedTime;
	}
	public void setStatusUpdatedTime(String statusUpdatedTime) {
		this.statusUpdatedTime = statusUpdatedTime;
	}
	@Override
	public int compareTo(Device o) {
		if(getLastActivityTime() != null && o.getLastActivityTime() != null)
		//return getLastActivityTime().compareTo(o.getLastActivityTime());
		{
			return o.getLastActivityTime().compareTo(getLastActivityTime());
		}
		return 0;
	}
	
	

}
