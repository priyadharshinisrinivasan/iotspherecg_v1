package com.ey.iot.iotspherecg.service;

import java.util.List;

import com.ey.iot.iotspherecg.model.AlertModel;

public abstract interface AlertService
{
  public abstract void savedata(AlertModel paramAlertModel);
  
  public abstract void deletedata(String paramString);
  
  public abstract List<AlertModel> finddata(String paramString);
  
}
