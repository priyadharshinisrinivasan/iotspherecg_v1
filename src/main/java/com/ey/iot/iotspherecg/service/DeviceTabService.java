package com.ey.iot.iotspherecg.service;

import java.util.List;

import com.ey.iot.iotspherecg.model.DeviceTabModel;

public abstract interface DeviceTabService
{
  public abstract void savedata(DeviceTabModel paramDeviceTabModel);
  
  public abstract void deletedata(String paramString);
  
  public abstract List<DeviceTabModel> finddata(String paramString);
  
}
