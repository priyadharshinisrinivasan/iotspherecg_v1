package com.ey.iot.iotspherecg.dao;

import java.util.List;

import com.ey.iot.iotspherecg.model.AlertModel;

public abstract interface AlertDao
{  
  public abstract List<AlertModel> finddata(String paramString);
  
  public abstract void deletedata(String paramString);
  public abstract void updateallfirmware(String status);
public abstract void saveOrUpdate(AlertModel jymodel);

public abstract List<AlertModel> checkregistration(String input);
}
