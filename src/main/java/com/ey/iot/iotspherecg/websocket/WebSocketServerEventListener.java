package com.ey.iot.iotspherecg.websocket;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.ey.iot.iotspherecg.model.DeviceMessage;

@Component
public class WebSocketServerEventListener {

	private static final Logger logger = LoggerFactory.getLogger(WebSocketServerEventListener.class);

	@Autowired
	private SimpMessageSendingOperations messagingTemplate;

	@EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new device over web socket connection:");
    }

	@EventListener
	public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
		StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

		String deviceId = (String) headerAccessor.getSessionAttributes().get("deviceId");
		if (deviceId != null) {
			logger.info("Device Disconnected : " + deviceId);

			DeviceMessage deviceMessage = new DeviceMessage();
			deviceMessage.setType(DeviceMessage.MessageType.LEAVE);
			deviceMessage.setSender(deviceId);
			deviceMessage.setContent(new JSONObject());

			messagingTemplate.convertAndSend("/channel/public", deviceMessage);
		}
	}
}
