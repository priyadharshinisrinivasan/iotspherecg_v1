package com.ey.iot.iotspherecg.websocket;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.ey.iot.iotspherecg.configuration.AppProperty;
import com.microsoft.azure.eventhubs.*;


@Component
public class MessageRead_IotHub {
	
	/*MessageRead_IoTHub read data from IoTHub and pass it to 
	Local Websocket using WSClient_IoTHub class.*/
	
	private static  Logger logger = Logger.getLogger(MessageRead_IotHub.class);
	//private static ConnectionStringBuilder connStr;
	private  static String namespacename;
	private  static String eventhubname;
	private  static String saskeyname;
	private  static String saskey;
	private static String consumerGroupName;
	private static ConnectionStringBuilder connStr;
	private EventHubClient client = null;
	public String websocket_uri;
	public static StompClientLocal ws_local;

	public MessageRead_IotHub() {
		System.out.println("Before java webapp iothub constructor");	
		this.websocket_uri = (new AppProperty()).getPropertyValue("websocket_uri");
					namespacename = (new AppProperty()).getPropertyValue("namespacename");
				eventhubname = (new AppProperty()).getPropertyValue("eventhubname");
				saskeyname = (new AppProperty()).getPropertyValue("saskeyname");
				saskey = (new AppProperty()).getPropertyValue("saskey");
			    consumerGroupName = (new AppProperty()).getPropertyValue("consumergroupname");
			    connStr = new ConnectionStringBuilder(namespacename, eventhubname,saskeyname, saskey);
			    //  connStr = new ConnectionStringBuilder(namespacename, eventhubname,saskeyname, consumerGroupName);
			//    Endpoint=amqps://iothub-ns-iotsphereh-220605-b56499ce75.servicebus.windows.net;EntityPath=iotspherehub;SharedAccessKeyName=iothubowner;SharedAccessKey=r/Yd4oLCCtkZIwy9STokSEU1a+l/cmtDI+9++DXB8O0=;OperationTimeout=PT1M;RetryPolicy=Default
	}

    private static EventHubClient receiveMessages(final String partitionId) {
        EventHubClient client = null;
        System.out.println("before receive message-IoTHub client");
        logger.info("before receive message-IoTHub client");
        try {
          client = EventHubClient.createFromConnectionStringSync(connStr.toString());
        } catch (Exception e) {
          System.out.println("Failed to create IoTHub client: " + e.getMessage());
  		logger.info("Failed to create IoTHub client: " + e.getMessage());
          System.exit(1);
        }
        try {
          // Create a receiver using the
          // default Event Hubs consumer group
          // that listens for messages from now on.
        	client.createReceiver(consumerGroupName, partitionId, Instant.now())
            .thenAccept(new Consumer<PartitionReceiver>() {
              public void accept(PartitionReceiver receiver) {
                System.out.println("** Created receiver on partition " + partitionId);
                logger.info("** Created receiver on partition " + partitionId);
                try {
                  while (true) {
                    Iterable<EventData> receivedEvents = receiver.receive(100).get();
                    int batchSize = 0;
                    if (receivedEvents != null) {
                    //  System.out.println("Got some events");
                      for (EventData receivedEvent : receivedEvents) {
                      /*  System.out.println(String.format("Offset: %s, SeqNo: %s, EnqueueTime: %s",
                          receivedEvent.getSystemProperties().getOffset(),
                          receivedEvent.getSystemProperties().getSequenceNumber(),
                          receivedEvent.getSystemProperties().getEnqueuedTime()));*/
                       // System.out.println(String.format("| Device ID: %s",
                         // receivedEvent.getSystemProperties().get("iothub-connection-device-id")));
                        String devicedata = new String(receivedEvent.getBytes(), Charset.defaultCharset());
                        ws_local.sendDeviceData(ws_local.stompSession, devicedata);                       
                        System.out.println(String.format("| Message Payload: %s",devicedata));
                        
                        batchSize++;
                      }
                    }
                   // System.out.println(String.format("Partition: %s, ReceivedBatch Size: %s", partitionId, batchSize));
                  }
                } catch (Exception e) {
                  System.out.println("Failed to receive messages: " + e.getMessage());
                  logger.info("Failed to receive messages: " + e.getMessage());
                }
              }
            });
          } catch (Exception e) {
            System.out.println("Failed to create receiver: " + e.getMessage());
            logger.info("Failed to create receiver: " + e.getMessage());
        }
        return client;
      }

	public String startClient(StompClientLocal ws_local) throws EventHubException, IOException, InterruptedException, ExecutionException {
		this.ws_local = ws_local;
		if (getClient() == null) {
			logger.info("Starting IoTHub MQTT Client-webapp");
			System.out.println("Insd Webapp java Starting IoTHub MQTT Client");
			setClient(receiveMessages("0"));//we can add more partitions here
			return "Client Started";
		} else
			//logger.info("IoT Hub Client-Webapp already running");
			//System.out.println("IoT Hub Client-Webapp already running");
			return "Client already running";
	}

	public String stopClient() {
		if (getClient() != null) {
			try {
				logger.info("Stopping IoTHub MQTT Client-Webapp");
				getClient().closeSync();
			} catch (EventHubException e) {
				logger.info("Error while stopping IoTHub MQTT Client");
				return "Error occured while closing";
			}
			setClient(null);
			return "Client stopped";
		} else
			return "Client Not Running";
	}

	public EventHubClient getClient() {
		return this.client;
	}

	public void setClient(EventHubClient client) {
		this.client = client;
	}

}
