<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Header in top div -->
<div class="container-flued">
	<div class="row" style="color: #FFF;">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="100" class="box1">
				<div id="layoutContainer" layout="row" ng-cloak layout-wrap
					layout-align="start" style="color: #000000;">
					<a href="#" style="color: #000000;"><bstyle="color: #000000;">Home</b></a>&nbsp;
					> &nbsp;<a href="#" style="color: #000000;"><b style="color: #000000;">Device</b></a>&nbsp;
					> &nbsp;<b style="color: #000000;">{{devices[currentDeviceIndex].name}}</b>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.water}">
				<i class="fa fa-tint"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.water}}</div>
			<div class="comment">Water</div>
			</rd-widget-body> </rd-widget>
		</div>
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.gas}">
				<i class="glyphicon glyphicon-scale"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.gas}}</div>
			<div class="comment">Gas</div>
			</rd-widget-body> </rd-widget>
		</div>
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.temp}">
				<i class="fa fa-fire"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.temp}}</div>
			<div class="comment">Temperature</div>
			</rd-widget-body> </rd-widget>
		</div>
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.motion}">
				<i class="fa fa-support"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.motion}}</div>
			<div class="comment">Motion</div>
			</rd-widget-body> </rd-widget>
		</div>
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.fire}">
				<i class="glyphicon glyphicon-fire"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.fire}}</div>
			<div class="comment">Fire</div>
			</rd-widget-body> </rd-widget>
		</div>
		<div class="col-lg-2 col-md-4 col-xs-12">
			<rd-widget> <rd-widget-body>
			<div class="widget-icon pull-left" ng-style="{'background-color': devices[currentDeviceIndex].alertcolor.smoke}">
				<i class="fa fa-cloud"></i>
			</div>
			<div class="title">{{devices[currentDeviceIndex].alert.smoke}}</div>
			<div class="comment">Smoke</div>
			</rd-widget-body> </rd-widget>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<nvd3 options="scatterChart" data="scatterChartdata"
				class=" dashboardchart"></nvd3>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="container-flued dashboardchart" style="height: 450px;">
				<div class="row">

					<div class="col-lg-6 col-md-6 col-xs-12">
						<span class="chart" easypiechart percent="tempPercent"
							options="tempOptions"> <span class="percent"
							ng-bind="tempPercent" title="Battery"></span>
						</span>
						<!-- <input type="range" min="0" max="100" step="1"
							ng-model="tempPercent" /> -->
					</div>
					<div class="col-lg-6 col-md-6 col-xs-12">
						<span class="chart" easypiechart percent="humidPercent"
							options="humidOptions"> <span class="percent"
							ng-bind="humidPercent" title="Risk"></span>
						</span>
						<!-- <input type="range" min="0" max="100" step="1"
							ng-model="waterPercent" /> -->
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12">
						<span class="chart" easypiechart percent="flowPercent"
							options="flowOptions"> <span class="percent"
							ng-bind="flowPercent" title="Daily Usage"></span>
						</span>
						<!-- <input type="range" min="0" max="100" step="1"
							ng-model="smokePercent" /> -->
					</div>
					<div class="col-lg-6 col-md-6 col-xs-12">
						<div class="container-flued">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<span class="chart" easypiechart percent="voltPercent"
										options="voltOptions"> <span class="percent"
										ng-bind="voltPercent" title="Stability"></span>
									</span>
									<!-- <input type="range" min="0" max="100" step="1"
							ng-model="motionPercent" /> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- <nvd3 options="pieChart" data="pieChartdata" class=" dashboardchart"></nvd3> -->
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<nvd3 options="pieChart" data="pieChartdata" class=" dashboardchart"></nvd3>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<nvd3 options="multiBarChart" data="multiBarChartdata"
				class=" dashboardchart"></nvd3>
		</div>
	</div>
</div>